package scraper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;

import scraper.Headlines.Headline;

public class YahooHeadlineScraper {

	private static final String HEADLINES_CLASS = "yfi_quote_headline";

	public static List<Headline> getHeadlines(Document doc) {

		List<Headline> headlines = new ArrayList<Headline>();
		SimpleDateFormat formatter = new SimpleDateFormat("EEEE, MMMM dd, yyyy");
		Date currentDate = null;

		// Grab the table holding the headlines
		Element innerBody = null;
		innerBody = doc.getElementsByClass(HEADLINES_CLASS).first();

		for (Node e : innerBody.childNodes()) {
			if (e instanceof Element) {
				Element ei = (Element) e;
				switch (ei.tagName()) {
				// h3 is used for dates
				case ("h3"):
					try {
						currentDate = formatter.parse(ei.text());

					} catch (ParseException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					break;
				// This contains headlines from that date
				case ("ul"):
					if (currentDate != null) {
						for (Element l : ei.children()) {

							Element link = l.getElementsByTag("a").first();
							Headline h = new Headline(link.text(),
									link.attr("href"), currentDate);
							headlines.add(h);

						}

					}
				}
			}

		}

		return headlines;
	}

}
