package scraper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import scraper.Headlines.Headline;

public class Main {

	public static void main(String[] args) {
		// if (args.length < 4) {
		// System.out.println("Missing arguments");
		// System.exit(0);
		// }

		String company = "AAPL";
		String fileName = "headlinesaapl.txt";
		String firstDate = "2015-04-29";
		String secondDate = "2015-04-25";

		// String company = args[0];
		// String fileName = args[1];
		// String firstDate = args[2];
		// String secondDate = args[3];

		// Input date format
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

		Date first = null;
		Date second = null;
		try {
			first = formatter.parse(firstDate);
			second = formatter.parse(secondDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		List<Headline> headlines = Headlines.getHeadlines(company, first,
				second);

		System.out.println("Got " + headlines.size() + " headlines");

		// Writing the output to json in correct format
		Headlines.HeadlinesContainer h = new Headlines.HeadlinesContainer();
		h.headlines = headlines.toArray(new Headline[headlines.size()]);
		JsonOutput.outputToFile(h, fileName, true);

	}

}
