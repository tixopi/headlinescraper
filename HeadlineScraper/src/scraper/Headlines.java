package scraper;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class Headlines {

	private static final String topURL = "http://finance.yahoo.com";
	private static final String baseURL = "http://finance.yahoo.com/q/h";

	static List<Headline> getHeadlines(String company, Date startDate,
			Date eDate) {

		ArrayList<Headline> headlines = new ArrayList<Headline>();

		Date currentDate = startDate;
		Date endDate = eDate;

		// Date format that yahoo uses
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

		System.out.println("Start date: " + formatter.format(currentDate));
		System.out.println("End date: " + formatter.format(endDate));

		// URL for most recent articles for start day
		String url = baseURL + "?s=" + company + "&t="
				+ formatter.format(currentDate);

		Document doc = null;
		try {

			// Keep fetching pages till we've reached end date
			while (currentDate.getTime() >= endDate.getTime()) {

				doc = Jsoup.connect(url).timeout(3000).get();

				// Extract the headlines from the page
				List<Headline> hs = YahooHeadlineScraper.getHeadlines(doc);
				if (!hs.isEmpty()) {
					System.out.println("Got headlines from "
							+ formatter.format(hs.get(0).date));
				}
				headlines.addAll(hs);

				// Set current date to last fetched headline date
				currentDate = headlines.get(headlines.size() - 1).date;

				// Get the url that older headlines points to
				url = doc.getElementsMatchingOwnText("Older Headlines").first()
						.attr("href");
				url = topURL + url;

			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		// Might have gone a few days over, remove extra ones
		filterHeadlines(endDate, headlines);

		return headlines;

	}

	/**
	 * Remove headlines that occurred before lastDate
	 */
	private static void filterHeadlines(Date lastDate, List<Headline> headlines) {

		Iterator<Headline> it = headlines.iterator();
		while (it.hasNext()) {
			if (it.next().date.getTime() < lastDate.getTime()) {
				it.remove();
			}
		}

	}

	public static class Headline {

		final String headline;
		final String url;
		final Date date;

		public Headline(String headline, String url, Date date) {
			this.headline = headline;
			this.url = url;
			this.date = date;
		}

	}

	// Used so gson gives the array a name
	public static class HeadlinesContainer {
		Headline[] headlines;
	}

}
