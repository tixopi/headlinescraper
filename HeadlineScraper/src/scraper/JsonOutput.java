package scraper;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Class for outputting to JSON
 *
 */
public class JsonOutput {

	/**
	 * Write the input in JSON format to a file. If overwrite is false this
	 * method will fail if a file already exists with that name.
	 * 
	 * @param object
	 *            Object to write
	 * @param fileName
	 *            Name of file to write
	 * @param If
	 *            false, will fail if file already exists
	 * 
	 * @return True if file written successfully, false otherwise
	 */
	public static <T> boolean outputToFile(T object, String fileName,
			boolean overWrite) {

		Path filePath = Paths.get(fileName);

		// If we aren't overwriting check if file already exists
		if (!overWrite && !Files.notExists(filePath)) {
			System.out.println("Error writing to " + fileName
					+ " : File already exists");
			return false;
		}

		// Use GSON library to convert to JSON
		// disableHTMLEscaping stops GSON from turning chars like apostrophes
		// into unicode escape sequences
		// http://stackoverflow.com/questions/4147012/can-you-avoid-gson-converting-and-into-unicode-escape-sequences/4147245#4147245
		// Quotes will still be escaped with \ to make it valid JSON
		GsonBuilder gsonBuilder = new GsonBuilder().disableHtmlEscaping();

		// Check if prettyprint flag has been set with --prettyprint
		// if (CLI.prettyPrint)
		gsonBuilder.setPrettyPrinting();

		Gson gson = gsonBuilder.create();
		String json = gson.toJson(object);

		try {

			// Create output folder(s) if don't already exist
			// Note there might not be any might just be using current working
			// directory
			Path outputFolder = filePath.getParent();
			if (outputFolder != null && !Files.exists(outputFolder)) {
				Files.createDirectories(outputFolder);
			}

			if (!overWrite) {
				// Create new file, fails if file already exists
				// Although we checked if file doesn't exist earlier, it could
				// have been created after that check, safer to use CREATE_NEW
				// option
				Files.write(filePath, json.getBytes(StandardCharsets.UTF_8),
						StandardOpenOption.CREATE_NEW, StandardOpenOption.WRITE);

			} else {
				// Create new file, overwrites file if already exists
				Files.write(filePath, json.getBytes(StandardCharsets.UTF_8));
				// System.out.println("JSONSTRING" + json);
				/*
				 * try (BufferedWriter writer =
				 * Files.newBufferedWriter(filePath, StandardCharsets.UTF_8)) {
				 * writer.write(json); }
				 */

			}

		} catch (IOException e) {
			System.out.println("Failed to write " + fileName);
			e.printStackTrace();
			return false;
		}

		System.out.println("Successfully wrote to " + filePath.toString());

		return true;

	}

	/**
	 * Create a new file and write the object in JSON format to it. This method
	 * will fail if a file already exists with that name.
	 * 
	 * @param object
	 * @param fileName
	 * 
	 * @return True if file written successfully, false otherwise
	 */
	public static <T> boolean outputToNewFile(T object, String fileName) {

		return outputToFile(object, fileName, false);

	}

}
